package ictgradschool.industry.lab10.ex04;

/**
 * Created by anhyd on 27/03/2017.
 */
public class ExerciseFour {

    /**
     * Returns the sum of all positive integers between 1 and num (inclusive).
     */
    public int getSum(int num) {

        // Implement a recursive solution to this method.

        if (num < 1 ){
            return 0;
        }

        return 2 * num - 1 + getSum(num - 2);

    }


    /**
     * Returns the smallest value in the given array, between the given first (inclusive) and second (exclusive) indices
     *
     * @param nums the array
     * @param firstIndex the inclusive lower index
     * @param secondIndex the exclusive upper index
     */
    public int getSmallest(int[] nums, int firstIndex, int secondIndex) {

        // Implement a recursive solution to this method.

        if (secondIndex - firstIndex == 1){
            return nums[firstIndex];
        }

        if (nums[firstIndex] < nums[secondIndex - 1]){
            secondIndex--;
        } else {
            firstIndex++;
        }

        return getSmallest(nums, firstIndex, secondIndex);
    }

    /**
     * Prints all ints from n down to 1.
     */
    public void printNums1(int n) {

        // Implement a recursive solution to this method.
        System.out.println(n);
        if (1 < n){
            printNums1(n - 1);
        }
    }

    /**
     * Prints all ints from 1 up to n.
     */
    public void printNums2(int n) {

        // Implement a recursive solution to this method.
        if (n > 1){
            printNums2(n - 1);
        }
        System.out.println(n);
    }

    /**
     * Returns the number of 'e' and 'E' characters in the given String.
     *
     * @param input the string to check
     */
    public int countEs(String input) {

        // Implement a recursive solution to this method.
        if (input.contains("E")) {
            input = input.replaceFirst("E","");
        } else if (input.contains("e")) {
            input = input.replaceFirst("e","");
        } else {return 0;}

        return countEs(input) + 1;
    }

    /**
     * Returns the nth number in the fibonacci sequence.
     */
    public int fibonacci(int n) {

        // Implement a recursive solution to this method.
        if (n == 1){
            return n;
        } else if (n == 0){
            return n;
        }

        return fibonacci(n-1) + fibonacci(n-2);
    }

    /**
     * Returns true if the given input String is a palindrome, false otherwise.
     *
     * @param input the String to check
     */
    public boolean isPalindrome(String input) {

        // Implement a recursive solution to this method.

        int len = input.length()-1;
        if (len == 0 || len == -1){
            return true;
        }
        if (input.charAt(0) == input.charAt(len)){
            return isPalindrome(input.substring(1, len));
        }
        return false;
    }

}
